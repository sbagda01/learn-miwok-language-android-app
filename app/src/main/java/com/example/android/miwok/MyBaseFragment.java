package com.example.android.miwok;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MyBaseFragment extends Fragment {
    WordTranslationAdapter mTranslationsAdapter;

    public void setTranslationsAdapter(WordTranslationAdapter mTranslationsAdapter) {
        this.mTranslationsAdapter = mTranslationsAdapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.word_list, container, false);
        ListView listView = rootView.findViewById(R.id.list);
        listView.setAdapter(mTranslationsAdapter);
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayerFromWordTranslationsAdapter();
    }

    /**
     * Releases the media player from the adapter used in translations
     */
    private void releaseMediaPlayerFromWordTranslationsAdapter() {
        if (mTranslationsAdapter != null)
            mTranslationsAdapter.releaseMediaPlayer();
    }
}
