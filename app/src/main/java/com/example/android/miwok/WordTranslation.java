package com.example.android.miwok;

import android.media.Image;

public class WordTranslation {
    private String sourceWord;
    private String translatedWord;
    private int imageId = -1;
    private int soundResourceId = -1;


    public WordTranslation(String sourceWord, String translatedWord, int soundResourceId){
        this.sourceWord = sourceWord;
        this.translatedWord = translatedWord;
        this.soundResourceId = soundResourceId;
    }

    public WordTranslation(
            String sourceWord,
            String translatedWord,
            int imageID,
            int soundResourceId){
        this.sourceWord = sourceWord;
        this.translatedWord = translatedWord;
        this.imageId = imageID;
        this.soundResourceId = soundResourceId;
    }

    public String getSourceWord() {
        return sourceWord;
    }

    public String getTranslatedWord() {
        return translatedWord;
    }

    public int getImageId() {
        return imageId;
    }

    public boolean hasImage(){
        return this.imageId >= 0;
    }

    public int getSoundResourceId() {
        return this.soundResourceId;
    }

    public boolean hasSound(){
        return this.soundResourceId >= 0;
    }
}