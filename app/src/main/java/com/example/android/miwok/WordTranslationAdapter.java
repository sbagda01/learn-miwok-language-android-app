package com.example.android.miwok;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WordTranslationAdapter extends ArrayAdapter<WordTranslation> {

    int mColorResourceId;
    MediaPlayer mMediaPlayer;
    AudioManager mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
    AudioManager.OnAudioFocusChangeListener afChangeListener;


    public WordTranslationAdapter(
            @NonNull Context context,
            @NonNull ArrayList<WordTranslation> objects,
            int colorResourceId) {
        // We don't use a predefined resource here so it can be zero.
        super(context, 0, objects);
        mColorResourceId = colorResourceId;

        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                // Check AudioManager to see what these states mean.
                if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                        focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                    mMediaPlayer.pause();
                    mMediaPlayer.seekTo(0);
                } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                    releaseMediaPlayer();
                } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                    mMediaPlayer.start();
                }
            }
        };
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the listItemView
        View listItemView = convertView;
        // Inflate a new listItemView using translation_layout as an item
        // if it does not already exist
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.translation_layout, parent, false);
        }
        // Get the {@link WordTranslation} object located at this position in the list
        final WordTranslation currentWordTranslation = getItem(position);

        // Set the values for both text views and the image of the currentWordTranslation
        TextView sourceWordTextView = listItemView.findViewById(R.id.source_word);
        sourceWordTextView.setText(currentWordTranslation.getSourceWord());

        TextView translatedWordTextView = listItemView.findViewById(R.id.dest_word);
        translatedWordTextView.setText(currentWordTranslation.getTranslatedWord());

        // Set the image only if it is available, hide otherwise.
        ImageView image = listItemView.findViewById(R.id.translation_image);
        if (currentWordTranslation.hasImage()) {
            image.setImageResource(currentWordTranslation.getImageId());
            image.setVisibility(View.VISIBLE);
        } else {
            image.setVisibility(View.GONE);
        }

        // Set the background color of the text container.
        int color = ContextCompat.getColor(getContext(), mColorResourceId);
        View wordsContainer = listItemView.findViewById(R.id.words_container);
        wordsContainer.setBackgroundColor(color);

        // Create a media player and set OnClickListener to this item to start/stop the playback.
        if (currentWordTranslation.hasSound()) {
            wordsContainer.setOnTouchListener(new View.OnTouchListener() {
                // Events run in an event loop and not in parallel so
                // there is no race condition on mMediaPlayer. Which means on each
                // click the onClick creates a new player and attaches listeners before the
                // next one can do so.
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    // Release before creating a new player to
                    // avoid 2 sounds playing at once.
                    releaseMediaPlayer();
                    mMediaPlayer = MediaPlayer.create(
                            getContext(),
                            currentWordTranslation.getSoundResourceId()
                    );
                    mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            // Release each player's resources on completion.
                            releaseMediaPlayer();
                        }
                    });
                    startAudioWithRespect();
                    return false;
                }
            });
        }
        return listItemView;
    }

    public void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            // Release the Media player resources.
            mMediaPlayer.release();
            // setting the media player to null is an easy way to tell that the media player
            // is not configured to play an audio file at the moment.
            mMediaPlayer = null;
        }
        // Regardless of whether or not we were granted audio focus, abandon it. This also
        // unregisters the AudioFocusChangeListener so we don't get anymore callbacks.
        mAudioManager.abandonAudioFocus(afChangeListener);
    }

    public void startAudioWithRespect() {
        // Request audio focus for playback
        int result = mAudioManager.requestAudioFocus(
                afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request transient focus.
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT
        );
        // But what if they are not granted??
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mMediaPlayer.start();
        }
    }
}