/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.miwok;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Provides the appropriate {@link Fragment} for a view pager.
 */
public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public SimpleFragmentPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        context = c;
    }


    private Fragment getNumbersFragment() {
        ArrayList<WordTranslation> englishNumbers = new ArrayList<WordTranslation>();
        WordTranslationAdapter itemsAdapter;

        englishNumbers.add(new WordTranslation(
                "one",
                "lutti",
                R.drawable.number_one,
                R.raw.number_one
        ));
        englishNumbers.add(new WordTranslation(
                "two",
                "otiiko",
                R.drawable.number_two,
                R.raw.number_two
        ));
        englishNumbers.add(new WordTranslation(
                "three",
                "tolookosu",
                R.drawable.number_three,
                R.raw.number_three
        ));
        englishNumbers.add(new WordTranslation(
                "four",
                "oyyisa",
                R.drawable.number_four,
                R.raw.number_four
        ));
        englishNumbers.add(new WordTranslation(
                "five",
                "massokka",
                R.drawable.number_five,
                R.raw.number_five
        ));
        englishNumbers.add(new WordTranslation(
                "six",
                "temmokka",
                R.drawable.number_six,
                R.raw.number_six
        ));
        englishNumbers.add(new WordTranslation(
                "seven",
                "kenekaku",
                R.drawable.number_seven,
                R.raw.number_seven
        ));
        englishNumbers.add(new WordTranslation(
                "eight",
                "kawinta",
                R.drawable.number_eight,
                R.raw.number_eight
        ));
        englishNumbers.add(new WordTranslation(
                "nine",
                "wo'e",
                R.drawable.number_nine,
                R.raw.number_nine
        ));
        englishNumbers.add(new WordTranslation(
                "ten",
                "na'aacha",
                R.drawable.number_ten,
                R.raw.number_ten
        ));

        itemsAdapter = new WordTranslationAdapter(
                context,
                englishNumbers,
                R.color.category_numbers
        );
        MyBaseFragment fragment = new MyBaseFragment();
        fragment.setTranslationsAdapter(itemsAdapter);
        return fragment;
    }

    private Fragment getFamilyFragment() {
        ArrayList<WordTranslation> familyTranslations = new ArrayList<WordTranslation>();
        WordTranslationAdapter familyTranslationsAdapter;
        familyTranslations.add(new WordTranslation(
                "father",
                "epe",
                R.drawable.family_father,
                R.raw.family_father
        ));
        familyTranslations.add(new WordTranslation(
                "mother",
                "eta",
                R.drawable.family_mother,
                R.raw.family_mother
        ));
        familyTranslations.add(new WordTranslation(
                "son",
                "angsi",
                R.drawable.family_son,
                R.raw.family_son
        ));
        familyTranslations.add(new WordTranslation(
                "daughter",
                "tune",
                R.drawable.family_daughter,
                R.raw.family_daughter
        ));
        familyTranslations.add(new WordTranslation(
                "older brother",
                "taachi",
                R.drawable.family_older_brother,
                R.raw.family_older_brother
        ));
        familyTranslations.add(new WordTranslation(
                "younger brother",
                "chaliti",
                R.drawable.family_younger_brother,
                R.raw.family_younger_brother
        ));
        familyTranslations.add(new WordTranslation(
                "older sister",
                "tete",
                R.drawable.family_older_sister,
                R.raw.family_older_sister
        ));
        familyTranslations.add(new WordTranslation(
                "younger sister",
                "kolliti",
                R.drawable.family_younger_sister,
                R.raw.family_younger_sister
        ));
        familyTranslations.add(new WordTranslation(
                "grandmother",
                "ama",
                R.drawable.family_grandmother,
                R.raw.family_grandmother
        ));
        familyTranslations.add(new WordTranslation(
                "grandfather",
                "paapa",
                R.drawable.family_grandfather,
                R.raw.family_grandfather
        ));

        familyTranslationsAdapter = new WordTranslationAdapter(
                context,
                familyTranslations,
                R.color.category_family
        );

        MyBaseFragment fragment = new MyBaseFragment();
        fragment.setTranslationsAdapter(familyTranslationsAdapter);

        return fragment;
    }

    private Fragment getColorsFragment() {
        ArrayList<WordTranslation> colorTranslations = new ArrayList<WordTranslation>();
        WordTranslationAdapter colorTranslationsAdapter;

        colorTranslations.add(new WordTranslation(
                "red",
                "wetetti",
                R.drawable.color_red,
                R.raw.color_red
        ));
        colorTranslations.add(new WordTranslation(
                "green",
                "chokokki",
                R.drawable.color_green,
                R.raw.color_green

        ));
        colorTranslations.add(new WordTranslation(
                "brown",
                "takaakki",
                R.drawable.color_brown,
                R.raw.color_brown
        ));
        colorTranslations.add(new WordTranslation(
                "gray",
                "topoppi",
                R.drawable.color_gray,
                R.raw.color_gray
        ));
        colorTranslations.add(new WordTranslation(
                "black",
                "kululli",
                R.drawable.color_black,
                R.raw.color_black
        ));
        colorTranslations.add(new WordTranslation(
                "white",
                "kelelli",
                R.drawable.color_white,
                R.raw.color_white
        ));
        colorTranslations.add(new WordTranslation(
                "dusty yellow",
                "topiisa",
                R.drawable.color_dusty_yellow,
                R.raw.color_dusty_yellow
        ));
        colorTranslations.add(new WordTranslation(
                "mustard yellow",
                "chiwiite",
                R.drawable.color_mustard_yellow,
                R.raw.color_mustard_yellow
        ));

        colorTranslationsAdapter = new WordTranslationAdapter(
                context,
                colorTranslations,
                R.color.category_colors
        );

        MyBaseFragment fragment = new MyBaseFragment();
        fragment.setTranslationsAdapter(colorTranslationsAdapter);
        return fragment;
    }

    private Fragment getPhrasesFragment() {
        ArrayList<WordTranslation> phrasesTranslations = new ArrayList<WordTranslation>();
        WordTranslationAdapter phrasesTranslationsAdapter;
        phrasesTranslations.add(new WordTranslation(
                "Where are you going?",
                "minto wuksus",
                R.raw.phrase_where_are_you_going
        ));
        phrasesTranslations.add(new WordTranslation(
                "What is your name?",
                "tinne oyaasene",
                R.raw.phrase_what_is_your_name
        ));
        phrasesTranslations.add(new WordTranslation(
                "My name is",
                "oyaaset",
                R.raw.phrase_my_name_is
        ));
        phrasesTranslations.add(new WordTranslation(
                "How are you feeling?",
                "michekses?",
                R.raw.phrase_how_are_you_feeling
        ));
        phrasesTranslations.add(new WordTranslation(
                "I'm feeling good.",
                "kuchi achit",
                R.raw.phrase_im_feeling_good
        ));
        phrasesTranslations.add(new WordTranslation(
                "Are you coming?",
                "eenes'aa",
                R.raw.phrase_are_you_coming
        ));
        phrasesTranslations.add(new WordTranslation(
                "Yes, I'm coming?",
                "hee'eenem",
                R.raw.phrase_yes_im_coming
        ));
        phrasesTranslations.add(new WordTranslation(
                "I'm coming",
                "eenem",
                R.raw.phrase_im_coming
        ));
        phrasesTranslations.add(new WordTranslation(
                "Let's go.",
                "yoowutis",
                R.raw.phrase_lets_go
        ));
        phrasesTranslations.add(new WordTranslation(
                "Come here.",
                "enni'nem",
                R.raw.phrase_come_here
        ));

        phrasesTranslationsAdapter = new WordTranslationAdapter(
                context,
                phrasesTranslations,
                R.color.category_phrases
        );

        MyBaseFragment fragment = new MyBaseFragment();
        fragment.setTranslationsAdapter(phrasesTranslationsAdapter);
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return getNumbersFragment();
        } else if (position == 1) {
            return getFamilyFragment();
        } else if (position == 2) {
            return getColorsFragment();
        } else {
            return getPhrasesFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String tabTitles[] = new String[]{"Numbers", "Family", "Colors", "Phrases"};
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return 4;
    }
}
